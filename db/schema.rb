# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140902165346) do

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "ad_categories", force: true do |t|
    t.string   "name"
    t.string   "alias"
    t.string   "icon_file_name"
    t.string   "icon_content_type"
    t.integer  "icon_file_size"
    t.datetime "icon_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ad_subcategories", force: true do |t|
    t.string   "name"
    t.string   "alias"
    t.integer  "ad_categorie_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ad_subcategories", ["ad_categorie_id"], name: "index_ad_subcategories_on_ad_categorie_id"

  create_table "add_texts", force: true do |t|
    t.string   "label"
    t.string   "description"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

# Could not dump table "admin_users" because of following NoMethodError
#   undefined method `[]' for nil:NilClass

  create_table "ads", force: true do |t|
    t.string   "name"
    t.text     "introtext"
    t.string   "price"
    t.string   "contact"
    t.integer  "ad_categorie_id"
    t.integer  "ad_subcategorie_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "per_name"
    t.string   "per_mail"
    t.string   "per_phone"
    t.integer  "moderation"
  end

  add_index "ads", ["ad_categorie_id"], name: "index_ads_on_ad_categorie_id"
  add_index "ads", ["ad_subcategorie_id"], name: "index_ads_on_ad_subcategorie_id"

  create_table "articles", force: true do |t|
    t.string   "title"
    t.text     "introtext"
    t.text     "content"
    t.string   "alias"
    t.integer  "likes"
    t.string   "preview_file_name"
    t.string   "preview_content_type"
    t.integer  "preview_file_size"
    t.datetime "preview_updated_at"
    t.integer  "rubric_id"
    t.integer  "specrubric_id"
    t.integer  "journals_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "tags"
    t.string   "similar_articles"
  end

  add_index "articles", ["journals_id"], name: "index_articles_on_journals_id"
  add_index "articles", ["rubric_id"], name: "index_articles_on_rubric_id"
  add_index "articles", ["specrubric_id"], name: "index_articles_on_specrubric_id"

  create_table "banners", force: true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "link"
    t.string   "label"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type"

  create_table "comments", force: true do |t|
    t.text     "comment_body"
    t.integer  "user_id"
    t.integer  "parent_id",    default: 0
    t.integer  "article_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contacts", force: true do |t|
    t.string   "phone_code"
    t.string   "phone"
    t.string   "address"
    t.string   "build"
    t.string   "email"
    t.string   "timetable"
    t.string   "path_car_file_name"
    t.string   "path_car_content_type"
    t.integer  "path_car_file_size"
    t.datetime "path_car_updated_at"
    t.string   "path_bus_file_name"
    t.string   "path_bus_content_type"
    t.integer  "path_bus_file_size"
    t.datetime "path_bus_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "journals", force: true do |t|
    t.string   "name"
    t.text     "content"
    t.string   "alias"
    t.string   "preview_file_name"
    t.string   "preview_content_type"
    t.integer  "preview_file_size"
    t.datetime "preview_updated_at"
    t.string   "pdf_file_name"
    t.string   "pdf_content_type"
    t.integer  "pdf_file_size"
    t.datetime "pdf_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "swf_file_name"
    t.string   "swf_content_type"
    t.integer  "swf_file_size"
    t.datetime "swf_updated_at"
  end

  create_table "partners", force: true do |t|
    t.string   "name"
    t.string   "link"
    t.string   "preview_file_name"
    t.string   "preview_content_type"
    t.integer  "preview_file_size"
    t.datetime "preview_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rubrics", force: true do |t|
    t.string   "name"
    t.string   "alias"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "specrubrics", force: true do |t|
    t.string   "name"
    t.string   "alias"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

# Could not dump table "users" because of following NoMethodError
#   undefined method `[]' for nil:NilClass

  create_table "votes", force: true do |t|
    t.string   "vote_variant"
    t.integer  "vote_value",   default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
