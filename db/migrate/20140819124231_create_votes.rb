class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.string :vote_variant
      t.integer :vote_value, :default => 0

      t.timestamps
    end
  end
end
