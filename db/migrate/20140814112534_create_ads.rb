class CreateAds < ActiveRecord::Migration
  def change
    create_table :ads do |t|
      t.string :name
      t.text :introtext
      t.string :price
      t.string :contact
      t.belongs_to :ad_categorie, index: true
      t.belongs_to :ad_subcategorie, index: true
      t.has_attached_file :image 
      t.timestamps
    end
  end
end