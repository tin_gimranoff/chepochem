class AddModerationToAds < ActiveRecord::Migration
  def change
    add_column :ads, :moderation, :integer
  end
end
