class CreateAdCategories < ActiveRecord::Migration
  def change
    create_table :ad_categories do |t|
      t.string :name
      t.string :alias
      t.has_attached_file :icon
      
      t.timestamps
    end
  end
end
