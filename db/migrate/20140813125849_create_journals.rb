class CreateJournals < ActiveRecord::Migration
  def change
    create_table :journals do |t|
      t.string :name
      t.text :content
      t.string :alias
      t.has_attached_file :preview
      t.has_attached_file :pdf
      t.timestamps
    end
  end
end
