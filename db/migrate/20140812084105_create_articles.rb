require 'paperclip'
class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.text :introtext
      t.text :content
      t.string :alias
      t.integer :likes
      t.has_attached_file :preview
      t.belongs_to :rubric, index: true
      t.belongs_to :specrubric, index: true
      t.belongs_to :journals, index: true

      t.timestamps
    end
  end
end
