class CreateAdSubcategories < ActiveRecord::Migration
  def change
    create_table :ad_subcategories do |t|
      t.string :name
      t.string :alias
      t.belongs_to :ad_categorie, index: true
      
      t.timestamps
    end
  end
end