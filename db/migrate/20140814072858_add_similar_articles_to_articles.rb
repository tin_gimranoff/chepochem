class AddSimilarArticlesToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :similar_articles, :string
  end
end
