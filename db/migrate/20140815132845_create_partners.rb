#encoding: utf-8
class CreatePartners < ActiveRecord::Migration
  def change
    create_table :partners do |t|
      t.string :name
      t.string :link
      t.has_attached_file :preview
      
      t.timestamps
    end
  end
end