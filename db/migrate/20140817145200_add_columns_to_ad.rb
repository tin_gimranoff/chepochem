class AddColumnsToAd < ActiveRecord::Migration
  def change
    add_column :ads, :per_name, :string
    add_column :ads, :per_mail, :string
    add_column :ads, :per_phone, :string
  end
end
