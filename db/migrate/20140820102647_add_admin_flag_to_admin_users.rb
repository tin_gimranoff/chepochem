class AddAdminFlagToAdminUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :admin, :bool, :default => false
  end
end
