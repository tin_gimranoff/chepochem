class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :comment_body
      t.integer :user_id
      t.integer :parent_id, :default => 0
      t.integer :article_id

      t.timestamps
    end
  end
end
