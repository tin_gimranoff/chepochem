class AddColumnsToUser < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.has_attached_file :avatar
      t.text :about
      t.integer :sex
      t.integer :age
    end
  end
end
