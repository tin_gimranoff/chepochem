class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :phone_code
      t.string :phone
      t.string :address
      t.string :build
      t.string :email
      t.string :timetable
      t.has_attached_file :path_car
      t.has_attached_file :path_bus
      
      t.timestamps
    end
  end
end
