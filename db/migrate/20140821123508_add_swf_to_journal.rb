class AddSwfToJournal < ActiveRecord::Migration
  def change
    change_table :journals do |t|
      t.has_attached_file :swf
    end
  end
end
