class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.has_attached_file :image
      t.string :link
      t.string :label
      t.string :description

      t.timestamps
    end
  end
end
