class AddText < ActiveRecord::Base
   validates :label, :description, :content, presence: true
end
