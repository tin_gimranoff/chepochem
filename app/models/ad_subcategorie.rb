class AdSubcategorie < ActiveRecord::Base
   belongs_to :ad_categorie
   
   validates :name, :alias, :ad_categorie_id,   presence: true
end
