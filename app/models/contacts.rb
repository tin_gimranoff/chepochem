class Contacts < ActiveRecord::Base
   has_attached_file :path_car, :styles => { :thumb => "367x142>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"
   has_attached_file :path_bus, :styles => { :thumb => "367x142>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"
   
   validates_attachment_content_type :path_car, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
   validates_attachment_content_type :path_car, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
   
   validates :phone_code, :phone,  :address, :build, :email, :timetable,  presence: true
   
end
