class Rubric < ActiveRecord::Base
  
    has_many :article
    
    validates :name, :alias, presence: true
    validates :alias, uniqueness: true
end
