require 'paperclip'

class Article < ActiveRecord::Base
   belongs_to :rubric
   belongs_to :specrubric
   belongs_to :journals
   
   has_attached_file :preview, :styles => { :medium => "177x119>", :thumb => "116x72>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"
   
   validates_attachment_content_type :preview, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
   
   validates :title, :introtext, :content, :rubric_id, :specrubric_id, :alias, :journals_id, :tags,  presence: true
   validates :alias, uniqueness: true
end
