class AdCategorie < ActiveRecord::Base
  
  has_many :ad_subcategorie
  
  has_attached_file :icon, :styles => { :thumb => "20x27>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"
  validates_attachment_content_type :icon, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"], :presence => true
  validates :name, :alias, presence: true
  
end
