require 'paperclip'

class Journals < ActiveRecord::Base
    
    has_many :article
  
    has_attached_file :preview, :styles => { :medium => "205x289>", :thumb => "108x153>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"
    has_attached_file :pdf, :path => ":rails_root/public/:class/:attachment/:id/:basename.:extension", :url => "/:class/:attachment/:id/:basename.:extension"
    has_attached_file :swf, :path => ":rails_root/public/:class/:attachment/:id/:basename.:extension", :url => "/:class/:attachment/:id/:basename.:extension"
    validates_attachment_content_type :preview, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
    validates_attachment_content_type :pdf, :content_type => ["application/pdf", "application/x-pdf"]
    validates_attachment_content_type :swf, :content_type => ["application/x-shockwave-flash"]
    
    validates :name, :content, :alias, presence: true
end
