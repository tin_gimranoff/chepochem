class Comment < ActiveRecord::Base
  validates :comment_body, :user_id, :parent_id, :article_id, presence: true
end
