class Partners < ActiveRecord::Base
  has_attached_file :preview, :styles => { :thumb => "165x129>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"
  validates_attachment_content_type :preview, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  
  validates :name, :link,  presence: true
end
