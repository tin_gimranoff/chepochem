class Banner < ActiveRecord::Base
  has_attached_file :image, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  
  validates :link, :label, :description,   presence: true
end
