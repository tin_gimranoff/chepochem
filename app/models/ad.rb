#coding: utf-8
class Ad < ActiveRecord::Base
    belongs_to :ad_categorie
    belongs_to :ad_subcategorie
    
    #attr_accessible :per_name, :per_mail, :per_phone, :introtext, :ad_categorie, :ad_subcategorie
    
    has_attached_file :image, :styles => { :thumb => "165x129>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"
    validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
    
    validates :name, :introtext, :price, :contact, :ad_categorie_id, :ad_subcategorie_id, :per_name,   presence: true
    
    validates :moderation, inclusion: { in: [0, 1] }
    
    validates :per_mail, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, message: "Не правильный формат почты" }, allow_blank: true
    validates :per_phone, format: { with: %r{\A[+0-9]*\z}, message: "Не правильный формат телефона" }
    
end
