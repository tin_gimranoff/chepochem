class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  has_attached_file :avatar, :styles => { :small => "61x55>", :thumb => "140x127>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"
  validates_attachment_content_type :avatar, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  
  validates :name, presence: true
  
  #0 - Мужской, 1 - Женский
  validates :sex, inclusion: { in: [0, 1] }, allow_blank: true
  validates :age, :numericality => {:only_integer => true}, allow_blank: true

end
