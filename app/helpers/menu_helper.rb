module MenuHelper
  def is_current(cur_controller, cur_action, addition_condition, link, link_text)
      if controller.controller_name == cur_controller && controller.action_name == cur_action && (addition_condition == nil || addition_condition.to_s == params[:id].to_s)
          return '<li class="active">'+link_text+'<img src="/im/active-marker.png" /></li>'
      else
          return '<li><a href="'+link+'">'+link_text+'</a></li>'
      end
  end

  def is_current_textpage(cur_controller, cur_action, addition_condition)
    if controller.controller_name == cur_controller && controller.action_name == cur_action && (addition_condition == nil || addition_condition.to_s == params[:slug].to_s)
      return 'active'
    end
  end
end