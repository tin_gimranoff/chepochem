#coding: utf-8
class AjaxController < ApplicationController
  
  skip_before_action :verify_authenticity_token
  
  layout false
  
  def like
    
    post = Article.find(params[:post_id])
    if session[params[:post_id]]
      render :json => {likes: post.likes}
      return 
    end
    if post.likes 
      post.update_attributes(:likes => post.likes + 1)
    else
      post.update_attributes(:likes => 1)
    end
    session[params[:post_id]] = 1
    render :json => {likes: post.likes}
    return
  end
  
  def get_subcategories
    @sub_categories = AdSubcategorie.where('ad_categorie_id = ?', params[:categorie_id])
    render :json => @sub_categories.map{|u| [u.name, u.id]}
  end
  
  def vote
    vote = Vote.find(params[:vote])
    vote.vote_value = vote.vote_value + 1
    vote.save
    session[:vote] = 1
    render :json => {errors: false}
    return
  end
  
  def del_comment 
    if (user_signed_in? && current_user.admin.to_i == 1)
      Comment.destroy(params[:comment_id])
      render :json => {errors: false}
      return
    end
    render :json => {errors: true}
      return
  end
  
end