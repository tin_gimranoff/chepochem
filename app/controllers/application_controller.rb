class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :configure_devise_permitted_parameters, if: :devise_controller?
  before_filter :construct  
  def authenticate_admin_user!
    authenticate_user! 
    unless current_user.admin?
      flash[:alert] = "This area is restricted to administrators only."
      redirect_to root_path 
    end
  end
 
  def current_admin_user
    return nil if user_signed_in? && !current_user.admin?
    current_user
  end

  protected

  def configure_devise_permitted_parameters
    registration_params = [:name, :sex, :age, :avatar, :about, :email, :password, :password_confirmation]

    if params[:action] == 'update'
      devise_parameter_sanitizer.for(:account_update) { 
        |u| u.permit(registration_params << :current_password)
      }
    elsif params[:action] == 'create'
      devise_parameter_sanitizer.for(:sign_up) { 
        |u| u.permit(registration_params) 
      }
    end
  end

  
  private

    def construct
      @meta_title = Array.new
      @meta_title.push 'Че почем'
      if params[:ad]
          params.permit!
          @ad_form = Ad.new(params[:ad])
          @ad_form.moderation = 0
          if @ad_form.valid?
            @ad_form.save
            @forms_errors = 0
          else
            @forms_errors = 1
          end
      else
        @ad_form = Ad.new
      end
    end
  
end
