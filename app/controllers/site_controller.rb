#coding: utf-8

class SiteController < ApplicationController
  
  add_breadcrumb "Главная", :root_path
  
  layout 'application'
  layout 'view', :only => :view_journal
  layout 'empty', :only => :ad_export
  
  def index
    @journals = Journals.all.order("id DESC")
    if params[:number]
      @current_journal = Journals.where("alias = ?", params[:number])[0]
      add_breadcrumb @current_journal.name, journal_filter_path(params[:number])
      @meta_title.push @current_journal.name
    else 
       @meta_title.push "Главная"
       @current_journal = @journals[0]
    end
    @articles = Article.where("journals_id = ?", @current_journal.id).order("id DESC").paginate(:page => params[:page], :per_page => 7)
    @count_items_on_page = @articles.count - ((@articles.current_page.to_i-1)*7)
  end
  
  def rubric
    @rubric = Rubric.find_by_alias(params[:name])
    @page_header = @rubric.name
    @articles = Article.where("rubric_id = ?", @rubric.id).paginate(:page => params[:page], :per_page => 7)
    @count_items_on_page = @articles.count - ((@articles.current_page.to_i-1)*7)
    add_breadcrumb @rubric.name, rubric_filter_path(params[:name])
    @meta_title.push @rubric.name
    render :filter
  end
  
  def project
    @specrubric = Specrubric.find_by_alias(params[:name])
    @page_header = @specrubric.name
    @articles = Article.where("specrubric_id = ?", @specrubric.id).paginate(:page => params[:page], :per_page => 7)
    @count_items_on_page = @articles.count - ((@articles.current_page.to_i-1)*7)
    add_breadcrumb @specrubric.name, project_filter_path(params[:name])
    @meta_title.push @specrubric.name
    render :filter
  end
  
  def tag
    @page_header = params[:t]
    @articles = Article.where("tags LIKE ?", "%#{params[:t]}%").paginate(:page => params[:page], :per_page => 7)
    @count_items_on_page = @articles.count - ((@articles.current_page.to_i-1)*7)
    add_breadcrumb params[:t]
    @meta_title.push params[:t]
    render :filter
  end
  
  def archive
    @journals = Journals.all.order("id DESC")
    if params[:number]
        @current_journal = Journals.where("alias = ?", params[:number])[0]
    else
        @current_journal = @journals[0]
    end
    add_breadcrumb @current_journal.name, journal_details_path(params[:number])
    @meta_title.push @current_journal.name
  end
  
  def article
    @article = Article.where("alias = ?", params[:alias])[0]
    @comments = Comment.where('article_id = ? AND parent_id = 0', @article.id)
    add_breadcrumb @article.title, article_page_path(params[:alias])
    @meta_title.push @article.title
    if params[:comment]
      params.permit!
      @comment = Comment.new(params[:comment])
      if !@comment.parent_id 
        @comment.parent_id = 0
      end
      @comment.user_id = current_user.id
      @comment.article_id = @article.id
      @comment.save
      params[:comment] = nil
      @comment = nil
      #render :json => @comment
      #return
    end
    render :textpage
  end
  
  def ad
    if !params[:categorie] && !params['subcategorie']
     @categories = AdCategorie.all
     add_breadcrumb "Каталог объявлений", ad_path
     @meta_title.push "Каталог объявлений"
     render :ad_catalog
    else
     if params[:categorie] && !params[:subcategorie]
       add_breadcrumb "Каталог объявлений", ad_path
       @meta_title.push "Каталог объявлений"
       add_breadcrumb AdCategorie.find_by_alias(params[:categorie]).name, ad_path(AdCategorie.find_by_alias(params[:categorie]).alias)
       @meta_title.push AdCategorie.find_by_alias(params[:categorie]).name
       @ads = Ad.where("ad_categorie_id = ? AND moderation = 1", AdCategorie.find_by_alias(params[:categorie]).id).paginate(:page => params[:page], :per_page => 10)
       if @ads.count == 0
         redirect_to '/adverts'
         return
       else
         render :ad_list
         return
       end
     end
     
     if params[:categorie] && params[:subcategorie]
       category_id = AdCategorie.find_by_alias(params[:categorie]).id
       add_breadcrumb "Каталог объявлений", ad_path
       @meta_title.push "Каталог объявлений"
       add_breadcrumb AdCategorie.find_by_alias(params[:categorie]).name, ad_path(AdCategorie.find_by_alias(params[:categorie]).alias)
       @meta_title.push AdCategorie.find_by_alias(params[:categorie]).name
       add_breadcrumb AdSubcategorie.where("alias = ? AND ad_categorie_id = ?", params[:subcategorie], category_id)[0].name, ad_path(AdCategorie.find_by_alias(params[:categorie]).alias, AdSubcategorie.where("alias = ? AND ad_categorie_id = ?", params[:subcategorie], category_id)[0].alias)
       @meta_title.push AdSubcategorie.where("alias = ? AND ad_categorie_id = ?", params[:subcategorie], category_id)[0].name
       @ads = Ad.where("ad_categorie_id = ? AND ad_subcategorie_id = ? AND moderation = 1", category_id, AdSubcategorie.where("alias = ? AND ad_categorie_id = ?", params[:subcategorie], category_id)[0].id).paginate(:page => params[:page], :per_page => 10)
       if @ads.count == 0
         redirect_to '/adverts'
         return
       else
         render :ad_list
         return
       end
    end
    redirect_to '/404'
  end
  end
  
  def partners
    @partners = Partners.all
    add_breadcrumb "Партнеры", '/partners'
    @meta_title.push "Партнеры"
  end
  
  def contacts
    @contacts = Contacts.first
    add_breadcrumb "Контакты", '/contacts'
    @meta_title.push "Контакты"
  end
  
  def advertisers
    @article = AddText.where("label = 'advertisers'")[0]
    add_breadcrumb "Рекламодателям", '/advertisers'
    @meta_title.push "Рекламодателям"
  end
  
  def view_journal
    @journal = Journals.where('alias = ?', params[:number])[0]
    @meta_title.push @journal.name
    render :view_journal
  end
  
  def search
    @meta_title.push 'Результаты поиска'
    add_breadcrumb "Результаты поиска", search_path
    @page_header = "Результаты поиска"
    @articles = Article.where("content LIKE ? OR introtext LIKE ? OR title LIKE ?", "%#{params[:q]}%", "%#{params[:q]}%", "%#{params[:q]}%").paginate(:page => params[:page], :per_page => 7)
    @count_items_on_page = @articles.count - ((@articles.current_page.to_i-1)*7)
    render :filter
  end
  
  def profile
    @user = User.find(params[:id])
  end
  
  def ad_export
    @ads = Ad.all
    
    headers["Content-Type"] = "application/vnd.ms-excel; charset=utf-8"
    headers["Content-type"] = "application/x-msexcel; charset=utf-8"
    headers["Content-Disposition"] = "attachment; filename=abc.xls"
    headers["Expires"] = "0"
    headers["Cache-Control"] = "must-revalidate, post-check=0, pre-check=0"
    headers["Cache-Control"] = "private"

  end

end
