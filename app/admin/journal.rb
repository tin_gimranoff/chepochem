# coding: utf-8
ActiveAdmin.register Journals do

  menu label: "Номера"
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :name, :content, :alias, :preview, :pdf, :swf
  
  index do
    selectable_column
    id_column
    column :name
    column :alias
    column :created_at
    column :updated_at
    actions
  end
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  form do |f|
    f.inputs "Новая статья" do
      f.input :name
      f.input :content, :as => :ckeditor
      f.input :alias
      f.input :preview, :required => false, :as => :file
      f.input :pdf, :required => false, :as => :file
      f.input :swf, :required => false, :as => :file
    end
    f.actions
  end
end