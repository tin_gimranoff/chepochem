#coding: utf-8
ActiveAdmin.register Banner do

  menu label: "Баннеры"

  permit_params :image, :link, :label, :description
  
   index do
    selectable_column
    id_column
    column :link
    column :label
    column :description
    column :created_at
    column :updated_at
    actions
  end
  
  
  form do |f|
    f.inputs "Новая статья" do
      f.input :image, :required => true, :as => :file
      f.input :link
      f.input :label
      f.input :description
    end
    f.actions
  end
  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end


end
