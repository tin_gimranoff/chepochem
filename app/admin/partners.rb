# coding: utf-8
ActiveAdmin.register Partners do

  menu label: "Партнеры"
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :name, :link, :preview
  
  index do
    selectable_column
    id_column
    column :name
    column :link
    column :created_at
    column :updated_at
    actions
  end
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  form do |f|
    f.inputs "Новый партнер" do
      f.input :name
      f.input :link
      f.input :preview, :required => true, :as => :file
    end
    f.actions
  end
end
