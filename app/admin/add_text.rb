#coding: utf-8
ActiveAdmin.register AddText do


  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  
  menu label: "Дополнительные текстовые области"
  
  permit_params :label, :description, :content
  
  index do
    selectable_column
    id_column
    column :label
    column :description
    column :created_at
    column :updated_at
    actions
  end
  
  form do |f|
    f.inputs "Новая статья" do
      f.input :label
      f.input :description
      f.input :content, :as => :ckeditor
    end
    f.actions
  end
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end


end
