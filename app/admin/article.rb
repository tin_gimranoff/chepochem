# coding: utf-8
ActiveAdmin.register Article do

  menu parent: "Статьи"
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :title, :introtext, :content, :rubric_id, :specrubric_id, :alias, :preview, :journals_id, :tags, :similar_articles
  
 index do
    selectable_column
    id_column
    column :title
    column :rubric_id do |r|
      Rubric.find(r.rubric_id).name
    end
    column :specrubric_id do |r|
      Specrubric.find(r.specrubric_id).name
    end
    column :journals_id do |j|
      Journals.find(j.journals_id).name
    end
    column :alias
    column :likes
    column :tags
    column :created_at
    column :updated_at
    actions
  end
  
  form do |f|
    f.inputs "Новая статья" do
      f.input :title
      f.input :introtext, :as => :ckeditor
      f.input :content, :as => :ckeditor
      f.input :rubric_id, as: :select, collection: Rubric.all
      f.input :specrubric_id, as: :select, collection: Specrubric.all
      f.input :journals_id, as: :select, collection: Journals.all
      f.input :alias
      f.input :tags
      f.input :similar_articles
      f.input :preview, :required => false, :as => :file
    end
    f.actions
  end
  
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end


end
