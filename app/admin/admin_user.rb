# coding: utf-8
ActiveAdmin.register User do
  
  menu label: "Пользователи"
  
  permit_params :email, :password, :password_confirmation, :name, :sex, :age, :about

  index do
    selectable_column
    id_column
    column :email
    column :name
    column :age
    column :sex do |s|
      if s.sex == 1
        "Женский"
      end
      if s.sex == 0
        "Мужской"
      end
    end
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at
  filter :name

  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :name
      f.input :age
      f.input :sex, as: :select, collection: [['Не выбран', ''], ['Мужской', 0], ['Женский', 1]]
      f.input :password
      f.input :password_confirmation
      f.input :about, as: :ckeditor
    end
    f.actions
  end

end
