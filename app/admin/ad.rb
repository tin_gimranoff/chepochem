# coding: utf-8
ActiveAdmin.register Ad do

  menu parent: "Объявления"
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :name, :introtext, :price, :contact, :ad_categorie_id, :ad_subcategorie_id, :image, :per_name, :per_mail, :per_phone, :moderation
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  index do
    selectable_column
    id_column
    column :name
    column :price
    column :contact
    column :ad_categorie_id do |c|
      AdCategorie.find(c.ad_categorie_id).name
    end
    column :ad_subcategorie_id do |c|
      AdSubcategorie.find(c.ad_subcategorie_id).name
    end
    column :per_name
    column :per_mail
    column :per_phone
    column :moderation do |c|
      if c.moderation == 1
        "Да"
      else
        "Нет"
      end
    end
    column :created_at
    column :updated_at
    actions
  end
  
   form do |f|
    f.inputs "Новая категория" do
      f.input :per_name
      f.input :per_mail
      f.input :per_phone
      f.input :name
      f.input :introtext, :as => :ckeditor
      f.input :price
      f.input :contact
      f.input :ad_categorie_id, as: :select, collection: AdCategorie.all
      f.input :ad_subcategorie_id, as: :select, collection: AdSubcategorie.all.map {|u| [AdCategorie.find(u.ad_categorie_id).name+' - '+u.name, u.id]}
      f.input :image, :required => false, :as => :file
      f.input :moderation, as: :select, collection: [ ['Да', 1], ['Нет', 0] ]
    end
    f.actions
  end

end