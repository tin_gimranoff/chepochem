# coding: utf-8
ActiveAdmin.register AdSubcategorie do
  
  menu label: "Подкатегории"
  menu parent: "Объявления"
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :name, :alias, :ad_categorie_id
  
   index do
    selectable_column
    id_column
    column :name
    column :alias
    column :ad_categorie_id do |c|
      AdCategorie.find(c.ad_categorie_id).name
    end
    column :created_at
    column :updated_at
    actions
  end
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

 form do |f|
    f.inputs "Новая категория" do
      f.input :name
      f.input :alias
      f.input :ad_categorie_id, as: :select, collection: AdCategorie.all
    end
    f.actions
  end
end
