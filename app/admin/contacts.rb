#coding: utf-8
ActiveAdmin.register Contacts do

  menu label: "Контакты"
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :phone_code, :phone, :address, :build, :email, :timetable, :path_car, :path_bus
  
  
  index do
    selectable_column
    id_column
    column :phone_code
    column :phone
    column :address
    column :build
    column :email
    column :timetable
    column :created_at
    column :updated_at
    actions
  end
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  form do |f|
    f.inputs "Контакты" do
      f.input :phone_code
      f.input :phone
      f.input :address
      f.input :build
      f.input :email
      f.input :timetable
      f.input :path_car, :required => false, :as => :file
      f.input :path_bus, :required => false, :as => :file
    end
    f.actions
  end

end
