$(document).ready(function(){

    //Выравниваем колонки

    //var left_panel_height = $(".left-panel").height();
    //var main_panel_height = $(".main-panel").height();
    //var right_panel_height = $(".right-panel").height();

   // if(left_panel_height > main_panel_height)
    //    var height = left_panel_height;
    //else
    //    var height = main_panel_height;
    //
    //if(height < right_panel_height)
    //    height = right_panel_height;
    var height = $(".main-panel").height();
    $(".left-panel").css("height", height+17);
    //$(".main-panel").css("height", height-17);
    $(".right-panel").css("height", height+34);
    //Список номеров
    $(".number").bind('click', function(){
        var offset = $(this).offset();
        if($("#number-list").css('display') == 'none')
            $("#number-list").css({"top":offset.top+30,"left":offset.left+20}).fadeIn(200);
        else
            $("#number-list").fadeOut(200);
    });

    $(".send-btn").bind('click', open_form);

    $(".close-form").bind('click', function() {
        $(this).parent().hide();
        $("#overlay").hide();
    });
    
    var txt_size = $("h1").text().length;
    var str_size = txt_size*24;
    $("h1").css('background-position-x', str_size+"px");

	$(".likes").bind('click', function(){
		var post_id = $(this).attr('data-type');
		var El = this;
		$.ajax({
			type: "POST",
			url: "/ajax/like",
			data: "post_id="+post_id,
			dataType: "json"
		}).done(function(data) {
			$($(El).children()[0]).empty().append(data.likes);
		});
	});
	
	$("#ad_ad_categorie_id").change(function() {
    	$.ajax({
			type: "POST",
			url: "/ajax/get_subcategories",
			data: "categorie_id="+$("#ad_ad_categorie_id").val(),
			dataType: "json"
		}).done(function(data) {
			$("#ad_ad_subcategorie_id").empty();
			$.each(data, function(index, value) {
				console.log(value[1]);
				$("#ad_ad_subcategorie_id").append("<option value="+value[1]+">"+value[0]+"</option>");
			})
		});
    	$("#ad_ad_subcategorie_id").css('display', 'block');
    	$("#ad_ad_subcategorie_id_label").css({'display':'block', 'margin-top':'20px'});
    });
    
    $("#vote_btn").bind('click', function() {
    	var data = $("#vote_form").serialize();
    	if(!data.match(/^vote=[0-9]+$/))
    		return false;
	   	$.ajax({
				type: "POST",
				url: "/ajax/vote",
				data: data,
				dataType: "json"
		}).done(function(data) {
			console.log(data);
				window.location.reload();
		});
    	return false;
    });
	
});

function  getPageSize(){
    var xScroll, yScroll;

    if (window.innerHeight && window.scrollMaxY) {
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
    } else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    } else if (document.documentElement && document.documentElement.scrollHeight > document.documentElement.offsetHeight){ // Explorer 6 strict mode
        xScroll = document.documentElement.scrollWidth;
        yScroll = document.documentElement.scrollHeight;
    } else { // Explorer Mac...would also work in Mozilla and Safari
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }

    var windowWidth, windowHeight;
    if (self.innerHeight) { // all except Explorer
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }

    // for small pages with total height less then height of the viewport
    if(yScroll < windowHeight){
        pageHeight = windowHeight;
    } else {
        pageHeight = yScroll;
    }

    // for small pages with total width less then width of the viewport
    if(xScroll < windowWidth){
        pageWidth = windowWidth;
    } else {
        pageWidth = xScroll;
    }

    return [pageWidth,pageHeight,windowWidth,windowHeight];
}

function open_form() {
        $("#overlay").show();
        $("#pop-up").css('top', getPageSize()[3]/2-290);
        $("#pop-up").css('left', getPageSize()[2]/2-268);
        $("#pop-up").show(500);
}