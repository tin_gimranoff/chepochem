Rails.application.routes.draw do
  devise_for :users, :skip => [:sign_in]
  #ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  #devise_for :admin_users, ActiveAdmin::Devise.config
  #ActiveAdmin.routes(self)
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'site#index'
  post '/' => 'site#index'
  match 'ribric/:name' => 'site#rubric', :as => 'rubric_filter', via: [:get, :post]
  match 'project/:name' => 'site#project', :as => 'project_filter', via: [:get, :post]
  match 'journal/:number' => 'site#index', :as => 'journal_filter', via: [:get, :post]
  get 'journal/view/:number' => 'site#view_journal', :as => 'journal_view'
  match 'tag/' => 'site#tag', :as => 'tags_filter', via: [:get, :post]
  match 'archive(/:number)' => 'site#archive', :as => 'journal_details', via: [:get, :post]
  match 'article/:alias' => 'site#article', :as => 'article_page', via: [:get, :post]
  match 'adverts(/:categorie(/:subcategorie))' => 'site#ad', :as => 'ad', via: [:get, :post]
  match 'partners' => 'site#partners', via: [:get, :post]
  match 'contacts' => 'site#contacts', via: [:get, :post]
  match 'advertisers' => 'site#advertisers', via: [:get, :post]
  match 'search' => 'site#search', :as => 'search', via: [:get, :post]
  match '/user/:id' => 'site#profile', :as => 'profile', via: [:get, :post]
  get '/ad/export' => 'site#ad_export'
  
  post 'ajax/like' => 'ajax#like'
  post 'ajax/get_subcategories' => 'ajax#get_subcategories'
  post 'ajax/vote' => 'ajax#vote'
  post 'ajax/del_comment' => 'ajax#del_comment'
  
  mount Ckeditor::Engine => '/ckeditor'
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
